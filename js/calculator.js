var app = angular.module('age_calculator',[]);


app.controller('main_controller', function($scope){
	$scope.ageYear = 0;
	$scope.ageDay = 0;
	$scope.age = 0;
	
	$scope.yearNow = new Date().getFullYear();
	$scope.current_date = new Date().valueOf();

	$scope.calculate = function(){
		

		$scope.birth_date = new Date();
		$scope.birth_date.setDate($scope.day);
		$scope.birth_date.setMonth($scope.month - 1);
		$scope.birth_date.setFullYear($scope.year);

		
		var timeDiff = Math.abs(new Date().getTime() - $scope.birth_date.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

		$scope.ageDay = diffDays % 365;
		diffDays -= $scope.ageDay;
		diffDays /= 365;
		$scope.ageYear = diffDays;

	};
}); 